;;; topcoder-extension.el --- Topcoder extension

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 11 May 2021
;; Keywords: topcoder, tools
;; Package-Version: 0.0.1
;; Package-Requires: ((emacs "25.1"))
;; Homepage: https://gitlab.com/nerding_it/emacs-topcoder-extension

;; This file is not part of Emacs

;;        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
;;                    Version 2, December 2004

;; Copyright (C) 2021 nerding_it <virtualxi99@gmail.com>

;; Everyone is permitted to copy and distribute verbatim or modified
;; copies of this license document, and changing it is allowed as long
;; as the name is changed.

;;            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
;;   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

;;  0. You just DO WHAT THE FUCK YOU WANT TO.

;;; Commentary:

;; This is topcoder extension for Emacs

;;; Code:

(require 'request)
(require 'json)
(require 'cl-lib)

(defgroup topcoder-extension nil
  "https://topcoder.com"
  :group 'external
  :prefix "topcoder-extension-"
  :link '(url-link :tag "Homepage" "https://gitlab.com/nerding_it/emacs-topcoder-extension"))

(defcustom topcoder-extension-auth-client-id "NY97yLYz1wHg3DBtI7NMEEksfI34KkXB"
  "Client ID for Auth."
  :group 'topcoder-extension
  :type 'string
  :tag "Topcoder Auth Client ID")

(defcustom topcoder-extension-auth-audience-url "https://m2m.topcoder-dev.com/"
  "Auth audience URL."
  :group 'topcoder-extension
  :type 'string
  :tag "Topcoder Auth audience URL")

(defcustom topcoder-extension-challenges-url "https://api.topcoder-dev.com/v5/challenges"
  "URL for challenges."
  :group 'topcoder-extension
  :type 'string
  :tag "Topcoder API challenges URL")

(defcustom topcoder-extension-members-url "https://api.topcoder-dev.com/v4/members"
  "URL for members."
  :group 'topcoder-extension
  :type 'string
  :tag "Topcoder API members URL")

(defcustom topcoder-extension-gig-opportunities-url "https://topcoder.com/api/recruit/jobs"
  "URL for gig opportunities."
  :group 'topcoder-extension
  :type 'string
  :tag "Topcoder gig opportunities URL")

(defcustom topcoder-extension-device-auth-url "https://topcoder-dev.auth0.com/oauth/device/code"
  "URL for device authorization."
  :group 'topcoder-extension
  :type 'string
  :tag "Topcoder API device authorization URL")

(defcustom topcoder-extension-auth-token-url "https://topcoder-dev.auth0.com/oauth/token"
  "URL for getting Authentication Token."
  :group 'topcoder-extension
  :type 'string
  :tag "Topcoder API Authentication token URL")

(defcustom topcoder-extension-auth-scope "offline_access openid profile refresh_token"
  "Authentication scope for device auth."
  :group 'topcoder-extension
  :type 'string
  :tag "Topcoder API device authentication scope")

(defcustom topcoder-extension-auth-grant-type "urn:ietf:params:oauth:grant-type:device_code"
  "Grant type for authentication."
  :group 'topcoder-extension
  :type 'string
  :tag "Topcoder API device authentication grant type.")

(defcustom topcoder-extension-api-use-cache nil
  "Enable or disable cache."
  :group 'topcoder-extension
  :type 'boolean
  :tag "Topcoder API Caching.")

(defvar topcoder-extension-challenge-list nil
  "Variable to hold list of challenges.")

(defvar topcoder-extension-gig-opportunity-list nil
  "Variable to hold list of gig opportnities.")

(defvar topcoder-extension-api-device-code nil
  "Device code for authentication.")

(defvar topcoder-extension-api-auth-interval nil
  "Polling interval for authentication.")

(defvar topcoder-extension-api-access-token nil
  "Variable to hold access token value.")

(defvar topcoder-extension-api-polling-timer nil
  "Variable to hold timer object.")

(defvar topcoder-extension-api-etags (make-hash-table)
  "Hash table to store etags for api responses.")

(defvar topcoder-extension-user-name nil
  "Username of logged in user.")

(defun topcoder-extension-response-parser ()
  "Parser for Topcoder API response."
  (let ((json-object-type 'hash-table)
	(json-array-type 'list)
	(json-key-type 'symbol))
    (json-read)))

(defun topcoder-extension-start-device-auth ()
  "Start device authentication."
  (request
    topcoder-extension-device-auth-url
    :type "POST"
    :parser 'topcoder-extension-response-parser
    :headers '(("Content-Type" . "application/json")
	       ("Accept" . "application/json"))
    :data (json-encode
	   `(("client_id" . ,topcoder-extension-auth-client-id)
	     ("scope" . ,topcoder-extension-auth-scope)
	     ("audience" . ,topcoder-extension-auth-audience-url)))
    :success
    (cl-function (lambda (&key data &allow-other-keys)
		   (cl-assert (hash-table-p data))
		   (setq topcoder-extension-api-device-code (gethash 'device_code data))
		   (setq topcoder-extension-api-auth-interval (gethash 'interval data))
		   (pop-to-buffer "*Topcoder Authentication*")
		   (topcoder-extension-auth-mode)
		   (read-only-mode)
		   (let ((inhibit-read-only t))
		     (erase-buffer)
		     (beginning-of-buffer)
		     (insert (format "#+TITLE: Topcoder Authentication for device code %s" topcoder-extension-api-device-code))
		     (insert "\n")
		     (insert (format "* Opening authentication link %s from default browser\n" (gethash 'verification_uri_complete data))))
		   (topcoder-extension-poll-device-authorization)))
    :error
    (cl-function (lambda (&key error-thrown &key response &allow-other-keys)
		   (error (prin1-to-string response))
		   (error (prin1-to-string error-thrown))))))

(defun topcoder-extension-poll-device-authorization ()
  "Poll for access token."
  (request
    topcoder-extension-auth-token-url
    :type "POST"
    :parser 'topcoder-extension-response-parser
    :headers '(("Content-Type" . "application/json")
	       ("Accept" . "application/json"))
    :data (json-encode `(("grant_type" . ,topcoder-extension-auth-grant-type)
			 ("device_code" . ,topcoder-extension-api-device-code)
			 ("client_id" . ,topcoder-extension-auth-client-id)))
    :success
    (cl-function (lambda (&key data &allow-other-keys)
		   (cl-assert (hash-table-p data))
		   (setq topcoder-extension-api-device-code nil)
		   (setq topcoder-extension-api-auth-interval nil)
		   (setq topcoder-extension-api-polling-timer nil)
		   (pop-to-buffer "*Topcoder Authentication*")
		   (setq topcoder-extension-api-access-token (gethash 'access_token data))
		   (let ((inhibit-read-only t))
		     (end-of-line)
		     (setq topcoder-extension-user-name (cdr (assoc "handle" (topcoder-extension-decode-jwt-token topcoder-extension-api-access-token))))
		     (insert (format "* User %s logged in\n" topcoder-extension-user-name)))))
    :error
    (cl-function (lambda (&rest args &key error-thrown &allow-other-keys)
		   (cl-assert (hash-table-p (cl-getf args :data)))
		   (let* ((error-type (gethash 'error (cl-getf args :data))))
		     (cond ((string-equal error-type "slow_down")
			    (message "Increasing interval for polling")
			    (setq topcoder-extension-api-auth-interval (+ topcoder-extension-api-auth-interval 1000)))
			   ((string-equal error-type "authorization_pending")
			    (message "Running callback after %d seconds" topcoder-extension-api-auth-interval)
			    (setq topcoder-extension-api-polling-timer (run-at-time (format "%d sec" topcoder-extension-api-auth-interval) nil 'topcoder-extension-poll-device-authorization)))
			   ((or (string-equal error-type "invalid_grant") (string-equal error-type "expired_token"))
			    (pop-to-buffer "*Topcoder Authentication*")
			    (let ((inhibit-read-only t))
			      (end-of-line)
			      (insert "** Invalid token\n")))
			   ((string-equal error-type "access_denied")
			    (pop-to-buffer "*Topcoder Authentication*")
			    (let ((inhibit-read-only t))
			      (end-of-line)
			      (insert "** Access denied"))
			    (message "Access Denied"))))))))

(defun topcoder-extension-retrieve-all-challenges ()
  "Retrieve all challenges from Topcoder API."
  (request
    topcoder-extension-challenges-url
    :type "GET"
    :parser 'topcoder-extension-response-parser
    :headers `(("Accept" . "application/json")
	       ("Authorization" . ,(format "Bearer %s" topcoder-extension-api-access-token)))
    :success
    (cl-function (lambda (&key data &key response &allow-other-keys)
		   (setq topcoder-extension-challenge-list nil)
		   (dolist (item data)
		     (let ((id (gethash 'id item))
			   (name (gethash 'name item))
			   (type (gethash 'type item))
			   (registrants (format "%s" (gethash 'numOfRegistrants item)))
			   (phases (mapconcat 'identity
					      (mapcar
					       (lambda (phase) (gethash 'name phase))
					       (seq-filter
						(lambda (i) (equal (gethash 'isOpen i) t))
						(gethash 'phases item)))
					      ","))
			   (prizes (mapconcat 'identity
					      (mapcar
					       (lambda (prizeSet)
			 			 (let ((prizes (mapconcat
								(lambda (value) (format "$%d" value))
			 					(mapcar
								 (lambda (prize) (gethash 'value prize))
								 (gethash 'prizes prizeSet))
								",")))
			 			   (format "%s -> %s" (gethash 'type prizeSet) prizes)))
			 		       (gethash 'prizeSets item))
					      ",")))
		       (add-to-list 'topcoder-extension-challenge-list (list id (vconcat [] `(,name, type, registrants, prizes, phases, id))))))
		   (setq tabulated-list-entries (mapcar
						 (lambda (item) item) topcoder-challenge-list))
		   (tabulated-list-revert)))
    :error
    (cl-function (lambda (&key error-thrown &key response &allow-other-keys)
		   (prin1 error-thrown)))))

(defun topcoder-extension-retrieve-challenge-details (id)
  "Retrieve details about challenge by `ID'."
  (request
    (format "%s/%s" topcoder-extension-challenges-url id)
    :type "GET"
    :parser 'topcoder-extension-response-parser
    :headers `(("Accept" . "application/json")
	       ("Authorization" . ,(format "Bearer %s" topcoder-extension-api-access-token)))
    :success (cl-function (lambda (&key data &allow-other-keys)
			    (cl-assert (hash-table-p data))
			    (insert (format "#+TITLE: %s - %s\n\n" (gethash 'type data) (gethash 'name data)))
			    (insert (format "* Prizes\n"))
			    (org-table-create "2x2")
			    (org-table-next-field)
			    (insert "TYPE")
			    (org-table-next-field)
			    (insert "AMOUNT")
			    (org-table-previous-field)
			    (next-line)
			    (next-line)
			    (mapc
			     (lambda (prizeSet)
			       (insert (gethash 'type prizeSet))
			       (org-table-next-field)
			       (insert (mapconcat 'identity (mapcar
			       				     (lambda (prize) (format "$%d" (gethash 'value prize)))
			       				     (gethash 'prizes prizeSet)) ","))
			       (org-table-next-row)
			       (org-beginning-of-line)
			       (org-table-next-field))
			     (gethash 'prizeSets data))
			    (org-table-align)
			    (delete-region (line-beginning-position) (line-end-position))
			    (previous-line)
			    (org-table-insert-hline)
			    (next-line)
			    (next-line)
			    (insert "\n")
			    (insert (format "* Meta\n"))
			    (org-table-create "3x2")
			    (org-table-next-field)
			    (insert "Phase")
			    (org-table-next-field)
			    (insert "# Registrants")
			    (org-table-next-field)
			    (insert "# Submissions")
			    (org-table-next-field)
			    (insert (mapconcat 'identity
					       (mapcar
						(lambda (item) (gethash 'name item))
						(seq-filter
						 (lambda (item) (equal (gethash 'isOpen item) t))
						 (gethash 'phases data))) ","))
			    (org-table-next-field)
			    (insert (format "%d" (gethash 'numOfRegistrants data)))
			    (org-table-next-field)
			    (insert (format "%d" (gethash 'numOfSubmissions data)))
			    (org-table-align)
			    (org-table-insert-hline)
			    (next-line)
			    (next-line)
			    (insert "\n* Description\n")
			    (let ((description nil))
			      (setq description (replace-regexp-in-string "\n" "\n" (gethash 'description data)))
			      (insert (shell-command-to-string (format "pandoc -f markdown -t org <<< %s" description))))
			    (fundamental-mode)
			    (topcoder-extension-challenge-details-mode)
			    (make-variable-buffer-local 'topcoder-extension-challenge-id)
			    (setq topcoder-extension-challenge-id (gethash 'id data))))
    :error (cl-function (lambda (&key error-thrown &key response &allow-other-keys)
			  (error (prin1-to-string response))
			  (error (prin1-to-string error-thrown))))))

(defun topcoder-extension-challenge-register ()
  "Register for challenge."
  (interactive)
  (request
    (format "%s/%s/register" topcoder-extension-challenges-url topcoder-extension-challenge-id)
    :type "POST"
    :parser 'topcoder-extension-response-parser
    :headers `(("Accept" . "application/json")
  	       ("Authorization" . ,(format "Bearer %s" topcoder-extension-api-access-token)))
    :success (cl-function (lambda (&key data &allow-other-keys)
			    (message "Registration successful")))
    :error (cl-function (lambda (&key error-thrown &key response &allow-other-keys)
			  (message "Failed to register for challenge")
			  (error (prin1-to-string response))
  			  (error (prin1-to-string error-thrown))))))

;; TODO: Need to create major mode for displaying user challenges
(defun topcoder-extension-retrieve-user-challenges ()
  "Retrieve registered challenge of current user."
  (request
    (format "%s/%s/challenges" topcoder-extension-members-url topcoder-extension-user-name)
    :type "GET"
    :parser 'topcoder-extension-response-parser
    :params '(("filter" . "status=ACTIVE"))
    :headers `(("Accept" . "application/json")
  	       ("Authorization" . ,(format "Bearer %s" topcoder-extension-api-access-token)))
    :success (cl-function (lambda (&key data &allow-other-keys)
			    (prin1 data)))
    :error (cl-function (lambda (&key error-thrown &key response &allow-other-keys)
			  (error (prin1-to-string response))
  			  (error (prin1-to-string error-thrown))))))

(defun topcoder-extension-retrieve-gig-opportunities ()
  "Retrieve gig opportnities from Topcoder API."
  (request
    topcoder-extension-gig-opportunities-url
    :type "GET"
    :parser 'topcoder-extension-response-parser
    :headers `(("Accept" . "application/json"))
    :params '(("job_status" . 1))
    :success
    (cl-function (lambda (&key data &key response &allow-other-keys)
		   (setq topcoder-extension-gig-opportunity-list nil)
		   (dolist (item data)
		     (let ((id (format "%s" (gethash 'id item)))
			   (name (gethash 'name item))
			   (location (gethash 'city item))
			   (salary (format "$%s - $%s" (gethash 'min_annual_salary item) (gethash 'max_annual_salary item)))
			   (timezone (or (gethash 'value
					      (car
					       (seq-filter
						(lambda (tz) (string-equal (gethash 'field_name tz) "Timezone"))
						(gethash 'custom_fields item))))
					 "N/A"))
			   (hours (or (gethash 'value
					      (car
					       (seq-filter
						(lambda (hr) (string-equal (gethash 'field_name hr) "Hours per week"))
						(gethash 'custom_fields item))))
				      "N/A"))
			   (skills (or (gethash 'value
					      (car
					       (seq-filter
						(lambda (skill) (string-equal (gethash 'field_name skill) "Technologies Required"))
						(gethash 'custom_fields item))))
				       "N/A"))
			   (duration (or (gethash 'value
					      (car
					       (seq-filter
						(lambda (dur) (string-equal (gethash 'field_name dur) "Duration"))
						(gethash 'custom_fields item))))
					 "N/A"))
			   (details (or (gethash 'value
						 (car
						  (seq-filter
						   (lambda (det) (string-equal (gethash 'fieldname det) "Topcoder URL")))))
					"N/A")))
		       (add-to-list 'topcoder-extension-gig-opportunity-list (list id (vconcat [] `(,name, location, timezone, hours, salary, duration, skills, details, id))))))
		   (setq tabulated-list-entries (mapcar
						 (lambda (item) item) topcoder-extension-gig-opportunity-list))
		   (tabulated-list-revert)))
    :error
    (cl-function (lambda (&key error-thrown &key response &allow-other-keys)
		   (prin1 error-thrown)))))

(defun topcoder-extension-gigs ()
  "List gig opportnities."
  (interactive)
  (pop-to-buffer "*Topcoder Gigs*")
  (topcoder-extension-gig-listing-mode)
  (tablist-revert))

(defun topcoder-extension-challenges ()
  "List challenges."
  (interactive)
  (if topcoder-extension-api-access-token
      (progn
	(pop-to-buffer "*Topcoder Challenges*")
	(topcoder-extension-challenges-mode)
	(tablist-revert))
    (message "Please login to continue.")))

(defun topcoder-extension-challenge-details ()
  "Display challenge details."
  (interactive)
  (if topcoder-extension-api-access-token
      (progn
	(let ((id (tabulated-list-get-id)))
	  (pop-to-buffer (format "*%s*" (aref (tabulated-list-get-entry) 0)))
	  (topcoder-extension-retrieve-challenge-details id)))
    (message "Please login to continue.")))

(defun topcoder-extension-login ()
  "Login to Topcoder platform."
  (interactive)
  (topcoder-extension-start-device-auth))

(defun topcoder-extension-cancel-login ()
    "Terminate the login process."
  (interactive)
  (if (timerp topcoder-extension-api-polling-timer)
      (progn
	(pop-to-buffer "*Topcoder Authentication*")
	(kill-buffer)
	(cancel-timer topcoder-extension-api-polling-timer))))

(defun topcoder-extension-decode-jwt-token (token)
  "Decode jwt `TOKEN'."
  (let* ((base64-url (nth 1 (split-string token "\\.")))
	 (base64 (concat (replace-regexp-in-string "_" "/" (replace-regexp-in-string "-" "+" base64-url)) "=="))
	 (base64-data (base64-decode-string base64))
	 (json-data (json-read-from-string base64-data)))
    (mapcar
     (lambda (element)
       (let ((key (car element))
	     (value (cdr element))
	     (new-key nil))
	 (if (string-match-p "https" (symbol-name key))
	     (progn
	       (setq new-key (replace-regexp-in-string "https://[a-z0-9-]+.com/"  "" (symbol-name key)))
	       (setq new-key (nth (1- (length (split-string new-key "/"))) (split-string new-key "/")))
	       `(,new-key . ,value))
	   `(,key . ,value)))) json-data)))

(define-derived-mode topcoder-extension-challenge-details-mode org-mode "Topcoder Challenge Details"
  "Major mode for displaying Topcoder challenge details.")

(define-derived-mode topcoder-extension-auth-mode org-mode "Topcoder Authentication Mode"
  "Major mode for displaying authentication information")

(define-derived-mode topcoder-extension-challenges-mode tabulated-list-mode "Topcoder Challenges"
  "Major mode for displaying Topcoder challenges."
  (setq tabulated-list-format
  	[("Name" 40 t)
  	 ("Type" 10 t)
  	 ("Registrants" 30 t)
  	 ("Prizes" 50 t)
  	 ("Phase" 50 t)])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'topcoder-extension-retrieve-all-challenges nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

(define-derived-mode topcoder-extension-gig-listing-mode tabulated-list-mode "Topcoder Gigs"
  "Major mode for displaying gig opportunities."
  (setq tabulated-list-format
	[("Title" 60 t)
	 ("Location" 40 t)
	 ("Time Zone" 15 t)
	 ("Hours" 15 t)
	 ("Salary" 30 t)
	 ("Duration" 10 t)
	 ("Skills" 30 t)
	 ("Details" 30 t)])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'topcoder-extension-retrieve-gig-opportunities nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

(define-key topcoder-extension-challenges-mode-map (kbd "<return>") 'topcoder-extension-challenge-details)
(define-key topcoder-extension-challenge-details-mode-map (kbd "r") 'topcoder-extension-challenge-register)

(if (not (executable-find "pandoc"))
    (error "Please install pandoc"))

(provide 'topcoder-extension)

;;; topcoder-extension.el ends here
